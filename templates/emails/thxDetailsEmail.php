<h2 style="color:#006699;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
    Detalji transakcije:
</h2>
<p><strong><?= __('Autorizacioni kod: ', 'woocommerce-gateway-nestpay')?></strong><?=$authCode?></p>
<p><strong><?= __('Status transakcije: ', 'woocommerce-gateway-nestpay')?></strong><?=$paymentResponse?></p>
<p><strong><?= __('Kod statusa transakcije: ', 'woocommerce-gateway-nestpay')?></strong><?=$returnCode?></p>
<p><strong><?= __('Broj transakcije: ', 'woocommerce-gateway-nestpay')?></strong><?=$transId?></p>
