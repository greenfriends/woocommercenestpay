<header class="paymentTitle"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <h4>Detalji prodavca:</h4>
</header>
<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Ime prodavca: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Ime prodavca: ', 'woocommerce-gateway-nestpay')?>"><?=$companyName?></td>
    </tr>
    <tr>
        <th><?= __('Adresa: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Adresa: ', 'woocommerce-gateway-nestpay')?>"><?=$companyAddress?></td>
    </tr>
    <tr>
        <th><?= __('PIB: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('PIB: ', 'woocommerce-gateway-nestpay')?>"><?=$companyPib?></td>
    </tr>
    <tr>
        <th><?= __('MB: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('MB: ', 'woocommerce-gateway-nestpay')?>"><?=$companyMb?></td>
    </tr>
    </tbody>
</table>