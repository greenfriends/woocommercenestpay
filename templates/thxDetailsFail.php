<header class="paymentTitle" style="margin-top:50px;"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <h4><?= __('Transakcija NIJE uspela. Vasa platna kartica nije zaduzena. Molimo Vas da proverite podatke i ', 'woocommerce-gateway-nestpay')?>
    <a href="<?=wc_get_checkout_url()?>" title="<?=__('pokušajte ponovo', 'woocommerce-gateway-nestpay')?>"><?=__('pokušajte ponovo', 'woocommerce-gateway-nestpay')?></a>.</h4>
    <h4><?= __('Detalji transakcije: ', 'woocommerce-gateway-nestpay')?></h4>
</header>

<?php
$customerName = '';
$customerAddress = '';
$customerPostCode = '';
$customerCity = '';
$customerCountry = '';

if (isset($_GET['cancel_order'])) {
    $order = wc_get_order($orderId);
    if($order) {
    $customerName = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
    $customerAddress = $order->get_billing_address_1();
    $customerPostCode = $order->get_billing_postcode();
    $customerCity = $order->get_billing_city();
    $customerCountry = $order->get_billing_country();
    }
}
?>
<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Broj narudžbenice: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$orderId?></td>
    </tr>
<!--    <tr>-->
<!--        <th>--><?php //echo __('Datum transakcije: ', 'woocommerce-gateway-nestpay')?><!--</th>-->
<!--        <td>--><?php //echo $_POST['ipgTransactionId']?><!--</td>-->
<!--    </tr>-->
    <tr>
        <th><?= __('Status transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['txstatus'] ?? ''?></td>
    </tr>
    <tr>
        <th><?= __('Broj transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['TRANID'] ?? ''?></td>
    </tr>
    <tr>
        <th><?= __('Jedinstveni broj internet transakcije: ', 'woocommerce-gateway-nestpay')?></th>
        <td><?=$_POST['xid'] ?? ''?></td>
    </tr>
    </tbody>
</table>

<header class="paymentTitle">
    <h4><?= __('Detalji kupca: ', 'woocommerce-gateway-nestpay')?></h4>
</header>
<table class="shop_table shop_table_responsive customer_details">
    <tbody>
    <tr>
        <th><?= __('Ime kupca: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Full name: ', 'woocommerce-gateway-nestpay')?>"><?=$customerName?></td>
    </tr>
    <tr>
        <th><?= __('Adresa: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Address: ', 'woocommerce-gateway-nestpay')?>"><?=$customerAddress?></td>
    </tr>
    <tr>
        <th><?= __('Poštanski broj: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Postcode: ', 'woocommerce-gateway-nestpay')?>"><?=$customerPostCode?></td>
    </tr>
    <tr>
        <th><?= __('Grad: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('City: ', 'woocommerce-gateway-nestpay')?>"><?=$customerCity?></td>
    </tr>
    <tr>
        <th><?= __('Država: ', 'woocommerce-gateway-nestpay')?></th>
        <td data-title="<?= __('Country: ', 'woocommerce-gateway-nestpay')?>"><?=$customerCountry?></td>
    </tr>
    </tbody>
</table>
<style>
    .woocommerce-cart-form__contents, .cart-collaterals{display:none}
    .woocommerce-cart-form{color:white}
    .paymentTitle{color:black}
    .customer_details{color:black}
    .gf-seller-info{display:none}
</style>